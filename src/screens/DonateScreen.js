import React from 'react'
import { StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, Image } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/FontAwesome'

const DonateScreen = () => {
    return (
        <View style={{height: '100%'}}>
            <View style={styles.header}>
                    <View style={{flexDirection: 'row'}}>
                        <Image source={require('../assets/images/logo.png')} style={{width: 30, height: 30, alignSelf: 'center'}} />
                        <View style={{flexDirection: 'row', marginLeft: 10}}>
                            <Text style={{color: '#1D94A8', fontWeight: 'bold', fontSize: 20}}>TALI</Text> 
                            <Text style={{color: '#1D94A8', fontSize: 20}}>KASIH</Text>
                        </View>
                    </View>
                    <TouchableOpacity>
                        <Icon name='search' size={25} color='#1D94A8'/>
                    </TouchableOpacity>
            </View>

            <ScrollView>
                <View style={styles.container}>
                    <Text style={styles.text}>Amount<Text style={{color: 'red'}}>*</Text></Text>
                    <TextInput placeholder='e.g. 20.000.000' placeholderTextColor="gray" style={styles.input} />
                    <Text style={styles.text}>Name<Text style={{color: 'red'}}>*</Text></Text>
                    <TextInput placeholder='e.g. Dave' placeholderTextColor="gray" style={styles.input} />
                    <Text style={styles.text}>Message<Text style={{color: 'red'}}>*</Text></Text>
                    <TextInput placeholder='Give Them Support..' placeholderTextColor="gray" style={styles.input} multiLine numberOfLines={7} />
                    
                    <Text style={styles.textPayment}>Select Payment<Text style={{color: 'red'}}>*</Text></Text>

                    <View style={styles.paymentWrapper}>
                        <TouchableOpacity style={styles.paymentIconActive}>
                        {/* <Ionicons name="card-outline" size={50} color='#1D94A8' /> */}
                        <Icon name='credit-card' size={40} color='#000'/>
                            <Text style={styles.paymentText}>Credit/Debit Card</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.paymentIcon}>
                        {/* <Ionicons name="share-outline" size={20} color='#1D94A8' /> */}
                        <Icon name='university' size={40} color='#000'/>
                            <Text style={styles.paymentText}>Bank Transfer</Text>
                        </TouchableOpacity>
                    </View>

                    {/* Option 1 */}
                    <Text style={styles.text}>Card Number<Text style={{color: 'red'}}>*</Text></Text>
                    <TextInput placeholder='e.g. 1234 5678 9012 3456' placeholderTextColor="gray" style={styles.input} />

                    <View style={styles.bottomInputWrapper}>
                        <View style={styles.bottomInput}>
                            <Text style={styles.text}>Expiry Date<Text style={{color: 'red'}}>*</Text></Text>
                            <TextInput placeholder='MM/YY' placeholderTextColor="gray" style={styles.input} />
                        </View>
                        <View style={styles.bottomInput}>
                            <Text style={styles.text}>CVV<Text style={{color: 'red'}}>*</Text></Text>
                            <TextInput placeholder='123' placeholderTextColor="gray" style={styles.input} />
                        </View>
                    </View>

                    {/* Option 2 */}
                    {/* <View style={styles.transferWrapper}>
                        <Text style={styles.transferTitle}>Transfer To</Text>
                        <View style={styles.transferTextWrapper}>
                            <View>
                                <Text style={styles.transferText1}>Account Number</Text>
                                <Text style={styles.transferText2}>1234 5678 90</Text>
                            </View>
                            <TouchableOpacity>
                                <Text style={styles.transferCopy}>COPY</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.transferTextWrapper}>
                            <View>
                                <Text style={styles.transferText1}>Account Holder Name</Text>
                                <Text style={styles.transferText2}>Tali Kasih</Text>
                            </View>
                        </View>
                        <View style={styles.transferTextWrapper}>
                            <View>
                                <Text style={styles.transferText1}>Total Amount</Text>
                                <Text style={styles.transferText2}>Rp. 20.000.000</Text>
                            </View>
                            <TouchableOpacity>
                                <Text style={styles.transferCopy}>COPY</Text>
                            </TouchableOpacity>
                        </View>
                    </View> */}
                </View>
            </ScrollView>

            <TouchableOpacity style={styles.buttonWrapper}>
                <Text style={styles.buttonWrapperText}>DONATE</Text>
            </TouchableOpacity>
        </View>
    )
}

export default DonateScreen

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        backgroundColor: '#FFF',
        borderBottomColor: '#ddd',
        paddingHorizontal: 16,
        paddingVertical: 12,
        justifyContent: 'space-between'
    },
    container: {
        paddingTop: 30,
        paddingHorizontal: 16
    },
    text: {
        fontWeight: 'bold',
        marginBottom: 8
    },
    input: {
        paddingHorizontal: 16,
        paddingVertical: 11,
        borderBottomWidth: 1,
        borderColor: 'gray',
        marginBottom: 25
    },
    textPayment: {
        fontWeight: 'bold',
        marginBottom: 13
    },
    paymentWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 33,
    },
    paymentIcon: {
        width: '45%',
        height: 90,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 4,
        borderColor: 'gray',
    },
    paymentIconActive: {
        backgroundColor: 'rgb(215, 235, 238)',
        width: '45%',
        height: 90,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 4,
        borderColor: 'gray',
    },
    paymentText: {

    },
    bottomInputWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 74
    },
    bottomInput: {
        width: '45%'
    },
    buttonWrapper: {
        paddingVertical: 13,
        alignItems: 'center',
        backgroundColor: '#A43F3C',
        position: 'absolute',
        bottom: 0,
        width: '100%'
    },
    buttonWrapperText: {
        fontWeight: 'bold',
        fontSize: 16,
        color: 'white'
    },
    transferWrapper: {
        paddingTop: 24,
        paddingHorizontal: 21,
        paddingBottom: 48,
        backgroundColor: '#F4F4F4',
        marginBottom: 70
    },
    transferTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#1D94A8',
        marginBottom: 19
    },
    transferTextWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 20,
    },
    transferText1: {
        color: 'gray',
    },
    transferText2: {
        fontWeight: 'bold'
    },
    transferCopy: {
        fontWeight: 'bold'
    },
})
