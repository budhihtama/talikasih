import React from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/FontAwesome'


export default function MyProfileEdit() {
    return (
        <View>
            <ScrollView style={styles.container}>  
                <View style={styles.header}>
                        <View style={{flexDirection: 'row'}}>
                        <Icon name='arrow-left' size={20} color='#1D94A8'/>
                            <View style={{flexDirection: 'row', marginLeft: '46%'}}>
                                <Text style={{color: 'black', fontWeight: 'bold', fontSize: 15}}>Edit Profile</Text> 
                            </View>
                        </View>
                </View>
                <View style={styles.content}>
                    <Image style={{marginHorizontal: 88}} source={require('../assets/images/ImageProfile.png')}/>
                    <View style={{flexDirection: 'row', alignItems: 'baseline', alignSelf: 'center'}}>
                        <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Register')}>
                            <Text style={{color: '#1D94A8',marginBottom: 10, textDecorationLine: 'underline', marginTop: 10, fontWeight: 'bold'}}>
                    Change Profile Image
                            </Text>
                        </TouchableOpacity>
                </View>
                    <View>
                        <Text style={styles.text}>Name<Text style={{color: 'red'}}>*</Text></Text>
                        <TextInput style={styles.textProfile}>Luna</TextInput>
                        <Text style={styles.text}>Email<Text style={{color: 'red'}}>*</Text></Text>
                        <TextInput style={styles.textProfile}>luna@gmail.com</TextInput>
                        <Text style={styles.text}>New Password<Text style={{color: 'red'}}>*</Text></Text>
                        <TextInput style={styles.textProfile}>******</TextInput>
                        <Text style={styles.text}>Confirm New Password<Text style={{color: 'red'}}>*</Text></Text>
                        <TextInput style={styles.textProfile}>******</TextInput>
                    </View>
                        <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Register')}>
                            <Text style={{fontWeight: 'bold', textDecorationLine: 'underline', alignSelf: 'flex-end', marginTop: 10}}>Reset password</Text>
                        </TouchableOpacity>
                            <Text style={{color: '#A87B14', marginTop: 48}}>We need your bank account for campaign porpose</Text>
                    <View style={{marginBottom: 70}}>
                        <Text style={styles.text}>Bank Name<Text style={{color: 'red'}}>*</Text></Text>
                        <TextInput style={styles.textProfile}>Bank ABC</TextInput>
                        <Text style={styles.text}>Bank Account Number<Text style={{color: 'red'}}>*</Text></Text>
                        <TextInput style={styles.textProfile}>1234567</TextInput>
                    </View>

                </View>
            </ScrollView>
                    <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('MainNavigator')}
                    style={styles.button}>
                    <Text style={{color: 'white', fontSize: 20, textAlign: 'center'}}>SAVE CHANGES</Text>
                    </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderBottomColor: 'gray',
        paddingHorizontal: 16,
        paddingVertical: 10,
        justifyContent: 'space-between'
    },
    content: {
        paddingHorizontal: 16,
        marginVertical: 21,
    },
    text: {
        marginVertical: 20,
        color: 'black',
        fontWeight: 'bold',
    },
    textProfile: {
        paddingLeft: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#1D94A8',
        paddingBottom: 11,
    },
    button: {
        width: '100%',
        backgroundColor: '#A43F3C',
        paddingVertical: 10,
        marginTop: 57,
        position: 'absolute',
        bottom: 0,
      },
})