import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';



export default function MoreLogin() {
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                    <View style={{flexDirection: 'row'}}>
                        <Image source={require('../assets/images/logo.png')} style={{width: 30, height: 30, alignSelf: 'center'}} />
                        <View style={{flexDirection: 'row', marginLeft: 10}}>
                            <Text style={{color: '#1D94A8', fontWeight: 'bold', fontSize: 20}}>TALI</Text> 
                            <Text style={{color: '#1D94A8', fontSize: 20}}>KASIH</Text>
                        </View>
                    </View>
            </View>
                <TouchableOpacity style={{flexDirection: 'row', marginLeft: 23, borderBottomWidth: 0.5, paddingVertical: 20}}>
                <Icon name='search' size={25} color='black'/>
                <Text style={{marginLeft: 10}}>About</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', marginLeft: 23, borderBottomWidth: 0.5, paddingVertical: 20}}>
                <Icon name='phone' size={25} color='black'/>
                <Text style={{marginLeft: 10}}>Contact Us</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', marginLeft: 23, borderBottomWidth: 0.5, paddingVertical: 20}}>
                <Ionicons name='chatbubble-outline' size={25}/>
                <Text style={{marginLeft: 10}}>FAQ</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', marginLeft: 23, borderBottomWidth: 0.5, paddingVertical: 20}}>
                <Ionicons name='log-out-outline' size={25} color='#A43F3C'/>
                <Text style={{marginLeft: 10, color: '#A43F3C'}}>Logout</Text>
                </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
    },
    header: {
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        backgroundColor: '#FFF',
        borderBottomColor: 'gray',
        paddingHorizontal: 16,
        paddingVertical: 10,
        justifyContent: 'space-between'
    },
})
