import React from 'react'
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

export default function Login() {
    return (
        <View style={styles.container}>
            <View style={styles.logo}>
                <Image source={require('../assets/images/logo.png')} />
                  <View style={{flexDirection: 'row'}}>
                    <Text style={{color: '#1D94A8', fontWeight: 'bold', fontSize: 30}}>TALI</Text> 
                    <Text style={{color: '#1D94A8', fontSize: 30}}>KASIH</Text> 
                 </View>
                <Text style={{fontFamily: 'montserrat'}}>Find causes you truly care about</Text>
            </View>
            <View style={styles.contInput}>
                    <TextInput placeholder="Name" placeholderTextColor='gray' style={styles.formInput} />
                    <TextInput placeholder="Email" placeholderTextColor='gray' style={styles.formInput} />
                    <TextInput placeholder="Password" placeholderTextColor='gray' style={styles.formInput} />
                    <TextInput placeholder="Confirm Password" placeholderTextColor='gray' style={styles.formInput} />

            </View>
            <View>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('MainNavigator')}
                style={styles.button}>
                <Text style={{color: 'white', fontSize: 20, textAlign: 'center'}}>SIGN UP</Text>
              </TouchableOpacity>
            </View>

            <View style={{flexDirection: 'row', marginTop: 5, alignItems: 'baseline', alignSelf: 'center'}}>
              <Text>Already have an account?</Text>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Register')}>
                <Text style={{color: '#1D94A8',marginBottom: 120, textDecorationLine: 'underline', marginTop: 10, paddingLeft: 5}}>
                  Sign in
                </Text>
              </TouchableOpacity>
            </View>
            
              <TouchableOpacity style={{flexDirection: 'row', alignSelf: 'center'}}>
                <Icon name="google" size={30}/>
                <Text style={{fontSize: 23, fontFamily: 'montserrat', marginLeft: 10}}>
                  Continue with Google
                </Text>
              </TouchableOpacity>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F1EDE4',
        height: '100%',
        paddingHorizontal: 29,
    },
    logo: {
        alignItems: 'center',
        marginTop: 55,
        marginBottom: 30,
    },
    contInput: {
      flexDirection: 'column',
    },
    formInput: {
      backgroundColor: '#FCFCFC',
      borderBottomWidth: 1,
      marginBottom: 15,
      paddingHorizontal: 20,
    },
    button: {
      backgroundColor: '#A43F3C',
      paddingVertical: 10,
      borderRadius: 5,
      marginTop: 10
    },
})