import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, ScrollView } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import MyCampaignCard from '../components/MyCampaignCard'

export default function MyProfileCampaign() {
    return (
        <View>
             <View style={styles.header}>
                        <View style={{flexDirection: 'row'}}>
                        <Icon name='arrow-left' size={20} color='#1D94A8'/>
                            <View style={{flexDirection: 'row', marginLeft: '36%'}}>
                                <Text style={{color: 'black', fontWeight: 'bold', fontSize: 15}}>My Campaign (2)</Text> 
                            </View>
                        </View>
            </View>
            <MyCampaignCard />
        </View>
    )
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderBottomColor: 'gray',
        paddingHorizontal: 16,
        paddingVertical: 10,
        justifyContent: 'space-between',
        marginBottom: 24,
    },

})