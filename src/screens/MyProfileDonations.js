import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Dimensions } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import MyDonationCard from '../components/MyDonationCard'

const {height} = Dimensions.get('window');
const {width} = Dimensions.get('window');
const contHeight = height * 0.16;

export default function MyProfileDonations() {
    return (
        <View>

             <View style={styles.header}>
                        <View style={{flexDirection: 'row'}}>
                        <Icon name='arrow-left' size={20} color='#1D94A8'/>
                            <View style={{flexDirection: 'row', marginLeft: '36%'}}>
                                <Text style={{color: 'black', fontWeight: 'bold', fontSize: 15}}>My Donations (23)</Text> 
                            </View>
                        </View>
            </View>

            <View style={{height: '100%'}}>
                <TouchableOpacity
                    onPress={() => props.navigation.navigate('MainNavigator')}
                    style={styles.button}>
                    <Text style={{color: 'white', fontSize: 20, textAlign: 'center'}}>DONATE</Text>
                </TouchableOpacity>
                <MyDonationCard/>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderBottomColor: 'gray',
        paddingHorizontal: 16,
        paddingVertical: 10,
        justifyContent: 'space-between',
    },
    button: {
        width: '100%',
        backgroundColor: '#A43F3C',
        paddingVertical: 10,
        marginTop: 57,
        position: 'absolute',
        bottom: contHeight,
        zIndex: 10,
        
      },
})
