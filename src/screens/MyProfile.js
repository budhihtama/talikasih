import React from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

export default function MyProfile() {
    return (
        <View style={styles.container}>  
            <View style={styles.header}>
                    <View style={{flexDirection: 'row'}}>
                        <Image source={require('../assets/images/logo.png')} style={{width: 30, height: 30, alignSelf: 'center'}} />
                        <View style={{flexDirection: 'row', marginLeft: 10}}>
                            <Text style={{color: '#1D94A8', fontWeight: 'bold', fontSize: 20}}>TALI</Text> 
                            <Text style={{color: '#1D94A8', fontSize: 20}}>KASIH</Text>
                        </View>
                    </View>
                    <Icon name='search' size={25} color='#1D94A8'/>
            </View>
            <View style={styles.content}>
                <Image style={{marginHorizontal: 88}} source={require('../assets/images/ImageProfile.png')}/>
                <View style={{flexDirection: 'row', alignItems: 'baseline', alignSelf: 'center'}}>
                    <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Register')}>
                        <Text style={{color: '#1D94A8',marginBottom: 10, textDecorationLine: 'underline', marginTop: 10, fontWeight: 'bold'}}>
                  Edit Profile
                        </Text>
                    </TouchableOpacity>
            </View>
                <View>
                    <Text style={styles.text}>Name</Text>
                    <Text style={styles.textProfile}>Luna</Text>
                    <Text style={styles.text}>Email</Text>
                    <Text style={styles.textProfile}>luna@gmail.com</Text>
                    <Text style={styles.text}>Email Bank Info</Text>
                    <Text style={styles.textProfile}>BCA - ********457</Text>
                </View>
                <View style={{marginVertical: '20%'}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: 0.5, borderBottomColor: 'gray', paddingVertical: 10}}>
                        <Text style={{fontSize: 15}}>My Donations (23)</Text>
                        <Icon name='arrow-right' size={20}/>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: 0.5, borderBottomColor: 'gray', paddingVertical: 10}}>
                        <Text style={{fontSize: 15}}>My Campaign (2)</Text>
                        <Icon name='arrow-right' size={20}/>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        height: '100%',
    },
    header: {
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        backgroundColor: '#FFF',
        borderBottomColor: 'gray',
        paddingHorizontal: 16,
        paddingVertical: 10,
        justifyContent: 'space-between'
    },
    content: {
        paddingHorizontal: 16,
        marginVertical: 21,
    },
    text: {
        color: 'gray',
        marginVertical: 20,
    },
    textProfile: {
        paddingLeft: 20,
        fontWeight: 'bold'
    },
})
