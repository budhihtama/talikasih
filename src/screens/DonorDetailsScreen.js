import React, { useState } from 'react'
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, TextInput } from 'react-native';
import ReadMore from '../components/Readmore';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FillBar from '../components/FillBar';
import TimelineComponent from '../components/TimelineComponent';
import BlurScreen from '../components/BlurScreen';
import ShareComponent from '../components/ShareComponent';

const DonorDetailsScreen = () => {
    const [isShare, setIsShare] = useState(false)

    return (
        <View>
            <ScrollView>
                <View>
                    <View style={styles.image} />
                </View>

                <View style={styles.content}>
                    <Text style={styles.titleCampaign}>Title Campaign</Text>
                    <View style={styles.profileDetailCard}>
                        <Text style={styles.priceTag}>IDR 30.000.000</Text>
                        <Text style={styles.goalPriceText}>Raised from IDR 50.000.000 goal</Text>

                        <FillBar />

                        <View style={styles.profiles}>
                            <View style={{width: 37, height: 37, backgroundColor: 'red', marginRight: 9}} />
                            <View style={styles.profileText}>
                                <Text style={styles.profileNameText}>Profile Name</Text>
                                <Text style={styles.fundraiserText}>Fundraiser</Text>
                            </View>
                        </View>

                        <View style={styles.squareDetails}>
                            <View style={styles.squares}>
                                <Text style={styles.squareText}>100</Text>
                                <Text style={styles.squareText2}>Donations</Text>
                            </View>
                            <View style={styles.squares}>
                                <Text style={styles.squareText}>100</Text>
                                <Text style={styles.squareText2}>Share</Text>
                            </View>
                            <View style={styles.squares}>
                                <Text style={styles.squareText}>10</Text>
                                <Text style={styles.squareText2}>Days Left</Text>
                            </View>
                        </View>

                    </View>


                    <View style={styles.storyWrapper}>
                        <Text style={styles.storyTitle}>The Story</Text>
                        <ReadMore
                        numberOfLines={10}>
                            <Text style={styles.storyDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nunc pellentesque enim ultrices nunc. Pretium massa, vel viverra id mi sed sit. In faucibus leo etiam cras elit malesuada augue. Sagittis quisque non, nullam facilisis. Tempus cras nibh vitae vitae. Risus gravida arcu non a rhoncus suscipit a eu ultrices. Vestibulum, ut cursus pellentesque turpis ipsum arcu congue. Sit arcu, non gravida praesent turpis varius. Phasellus morbi donec pulvinar nisi ac augue at duis dolor. Sed ut hendrerit neque nunc accumsan ac massa. Nullam scelerisque Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nunc pellentesque enim ultrices nunc. Pretium massa, vel viverra id mi sed sit. In faucibus leo etiam cras elit malesuada augue. Sagittis quisque non, nullam facilisis. Tempus cras nibh vitae vitae. Risus gravida arcu non a rhoncus suscipit a eu ultrices. Vestibulum, ut cursus pellentesque turpis ipsum arcu congue. Sit arcu, non gravida praesent turpis varius. Phasellus morbi donec pulvinar nisi ac augue at duis dolor. Sed ut hendrerit neque nunc accumsan ac massa. Nullam scelerisque Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nunc pellentesque enim ultrices nunc. Pretium massa, vel viverra id mi sed sit. In faucibus leo etiam cras elit malesuada augue. Sagittis quisque non, nullam facilisis. Tempus cras nibh vitae vitae. Risus gravida arcu non a rhoncus suscipit a eu ultrices. Vestibulum, ut cursus pellentesque turpis ipsum arcu congue. Sit arcu, non gravida praesent turpis varius. Phasellus morbi donec pulvinar nisi ac augue at duis dolor. Sed ut hendrerit neque nunc accumsan ac massa. Nullam scelerisque</Text>
                        </ReadMore>
                    </View>

                    {/* Timeline */}
                    <View style={styles.timelineWrapper}>
                        <Text style={styles.timelineTitle}>Updates (10)</Text>
                        <TimelineComponent />
                        {/* <View style={styles.timelineText}>
                            <Text style={styles.timelineDescText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nunc pellentesque enim ultrices nunc. Pretium massa, vel viverra id mi sed sit. In faucibus leo etiam cras elit malesuada augue. Sagittis quisque non, nullam facilisis. Tempus cras nibh vitae vitae. Risus gravida arcu non a rhoncus suscipit a eu ultrices. Vestibulum, ut cursus pellentesque turpis ipsum arcu congue. Sit arcu, non gravida praesent turpis varius. Phasellus morbi donec pulvinar nisi ac augue at duis dolor. Sed ut hendrerit neque nunc accumsan ac massa. Nullam scelerisque</Text>
                        </View> */}
                        <TouchableOpacity style={styles.timelineButton}>
                            <Text style={styles.timelineButtonText}>Show Older</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.donationWrapper}>
                        <Text style={styles.donationTitle}>Donations (130)</Text>
                        <View style={styles.donationCard}>
                            <View style={styles.donationHeader}>
                                <View style={styles.donationImg} />
                                <View style={styles.donationProfile}>
                                    <Text style={styles.donationPrice}>Rp. 300.000</Text>
                                    <Text style={styles.donationName}>Justin Junasedi</Text>
                                </View>
                                <Text style={styles.donationTime}>12 Minutes ago</Text>
                            </View>
                            <Text style={styles.donationDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nunc pellentesque enim ultrices nunc. Pretium massa, vel viverra id mi sed sit. In faucibus leo etiam cras elit malesuada augue. Sagittis quisque non, nullam facilisis.</Text>
                        </View>
                        <View style={styles.donationCard}>
                            <View style={styles.donationHeader}>
                                <View style={styles.donationImg} />
                                <View style={styles.donationProfile}>
                                    <Text style={styles.donationPrice}>Rp. 300.000</Text>
                                    <Text style={styles.donationName}>Justin Junasedi</Text>
                                </View>
                                <Text style={styles.donationTime}>12 Minutes ago</Text>
                            </View>
                        </View>

                        <TouchableOpacity style={styles.timelineButton}>
                            <Text style={styles.timelineButtonText}>Load More</Text>
                        </TouchableOpacity>
                    </View>
                    
                    {/* Comments */}
                    <View style={styles.donationWrapper}>
                        <Text style={styles.donationTitle}>Comments (10)</Text>

                        <TextInput placeholder='Give Them Support...' placeholderTextColor='#aaa' multiline numberOfLines={5} style={styles.commentsInput} />

                        <TouchableOpacity style={styles.commentsButton}>
                            <Text style={styles.commentsButtonText}>POST</Text>
                        </TouchableOpacity>

                        <View style={styles.commentsCard}>
                            <View style={styles.donationHeader}>
                                <View style={styles.donationImg} />
                                <View>
                                    <Text style={styles.commentsName}>Steve Jobs</Text>
                                    <Text style={styles.commentsTime}>12 Minutes ago</Text>
                                </View>
                            </View>
                            <Text style={styles.donationDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nunc pellentesque enim ultrices nunc. Pretium massa, vel viverra id mi sed sit. In faucibus leo etiam cras elit malesuada augue. Sagittis quisque non, nullam facilisis.</Text>
                        </View>
                        <View style={styles.commentsCard}>
                            <View style={styles.donationHeader}>
                                <View style={styles.donationImg} />
                                <View>
                                    <Text style={styles.commentsName}>Steve Jobs</Text>
                                    <Text style={styles.commentsTime}>12 Minutes ago</Text>
                                </View>
                            </View>
                            <Text style={styles.donationDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nunc pellentesque enim ultrices nunc. Pretium massa, vel viverra id mi sed sit. In faucibus leo etiam cras elit malesuada augue. Sagittis quisque non, nullam facilisis.</Text>
                        </View>
                        <View style={styles.commentsCard}>
                            <View style={styles.donationHeader}>
                                <View style={styles.donationImg} />
                                <View>
                                    <Text style={styles.commentsName}>Steve Jobs</Text>
                                    <Text style={styles.commentsTime}>12 Minutes ago</Text>
                                </View>
                            </View>
                            <Text style={styles.donationDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nunc pellentesque enim ultrices nunc. Pretium massa, vel viverra id mi sed sit. In faucibus leo etiam cras elit malesuada augue. Sagittis quisque non, nullam facilisis.</Text>
                        </View>

                        <TouchableOpacity style={styles.timelineButton}>
                            <Text style={styles.timelineButtonText}>Show More</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
            <View style={styles.footer}>
                <TouchableOpacity style={styles.footerShare}
                onPress={() => setIsShare(true)}>
                <Ionicons name="share-outline" size={20} color='#1D94A8' />
                    <Text style={styles.footerShareText}>SHARE</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.footerDonate}>
                    <Text style={styles.footerDonateText}>DONATE</Text>
                </TouchableOpacity>
            </View>

            { isShare ? (
                <>
                <BlurScreen />
                <ShareComponent />
                </>
            ) : null }
            
        </View>
    )
}

export default DonorDetailsScreen

const styles = StyleSheet.create({
    content: {
        paddingHorizontal: 16,
        marginBottom: 50
    },
    image: {
        height: 225,
        backgroundColor: 'red',
        marginBottom: 23,
    },
    titleCampaign: {
        // textAlign: 'center',
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 22
    },
    profileDetailCard: {
        paddingHorizontal: 20,
        paddingTop: 17,
        borderWidth: 3,
        borderColor: '#F1EDE4',
        borderRadius: 4,
        marginBottom: 20
    },
    priceTag: {
        fontSize: 22,
        color: '#A43F3C',
        fontWeight: 'bold',
    },
    goalPriceText: {
        marginBottom: 5
    },
    profiles: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 17,
        marginTop: 16
    },
    profileText: {
        justifyContent: 'center'
    },
    profileNameText: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    fundraiserText: {
        color: 'grey'
    },
    squareDetails: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    squares: {
        width: 84, 
        height: 84, 
        backgroundColor: '#fff',
        elevation: 4,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        marginBottom: 33
    },
    squareText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#1D94A8'
    },
    squareText2: {
        color: 'grey'
    },
    storyWrapper: {
        paddingHorizontal: 20,
        paddingTop: 28,
        alignItems: 'center',
        paddingBottom: 20
    },
    storyTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 3
    },
    storyDesc: {
        lineHeight: 19,
        marginBottom: 25
    },
    timelineWrapper: {
        paddingHorizontal: 16,
        paddingTop: 16,
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius: 8,
        paddingBottom: 26,
        marginBottom: 15
    },
    timelineTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 20,
    },
    timelineText: {
        padding: 11,
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius: 8,
    },
    timelineDescText: {},
    timelineButton: {
        width: 150,
        borderWidth: 1,
        borderColor: '#1D94A8',
        borderRadius: 3,
        alignItems: 'center',
        marginTop: 10,
        alignSelf: 'center',
        paddingVertical: 6
    },
    timelineButtonText: {
        color: '#1D94A8',
        fontWeight: 'bold'
    },
    donationWrapper: {
        paddingHorizontal: 10,
        paddingTop: 24,
        paddingBottom: 30,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#ddd',
        marginBottom: 15
    },
    donationTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 18
    },
    donationCard: {
        paddingHorizontal: 12,
        paddingTop: 14,
        paddingBottom: 10,
        borderWidth: 1,
        borderBottomWidth: 3,
        borderRadius: 4,
        borderColor: '#ddd',
        marginBottom: 5,
    },
    donationHeader: {
        flexDirection: 'row',
        marginBottom: 11
    },
    donationImg: {
        width: 49,
        height: 46,
        backgroundColor: 'red',
        marginRight: 16,
    },
    donationProfile: {},
    donationPrice: {
        fontWeight: 'bold',
        color: '#1D94A8',
    },
    donationName: {
        fontSize: 14,
    },
    donationTime: {
        marginLeft: 'auto',
        marginRight: 0,
        color: 'grey',
        fontSize: 12
    },
    donationDesc: {
        lineHeight: 19
    },
    commentsCard: {
        paddingHorizontal: 12,
        paddingTop: 14,
        paddingBottom: 10,
        borderTopWidth: 1,
        borderRadius: 4,
        borderColor: '#ddd',
        marginBottom: 10,
    },
    commentsName: {
        fontWeight: 'bold'
    },
    commentsTime: {
        fontSize: 12,
        color: '#aaa'
    },
    commentsInput: {
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius: 5,
        marginBottom: 15
    },
    commentsButton: {
        alignSelf: 'flex-end',
        paddingVertical: 12,
        paddingHorizontal: 40,
        borderRadius: 4,
        marginBottom: 30,
        fontWeight: 'bold',
        fontSize: 16,
        backgroundColor: '#A43F3C'
    },
    commentsButtonText: {
        color: 'white'
    },
    footer: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        borderTopWidth: 1,
        borderColor: '#ddd',
        flexDirection: 'row',
        backgroundColor: 'white'
    },
    footerShare: {
        paddingVertical: 13,
        alignItems: 'center',
        justifyContent: 'center',
        width: '40%',
        backgroundColor: '#fff',
        flexDirection: 'row'
    },
    footerShareText: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#1d94a8',
        marginLeft: 5,
    },
    footerDonate: {
        paddingVertical: 13,
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#A43F3C'
    },
    footerDonateText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 16
    },
})
