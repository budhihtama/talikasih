import React from 'react'
import { StyleSheet, Text, View } from 'react-native';
import Progress from 'react-native-progress/Bar';

const FillBar = () => {
    return (
        <Progress progress={0.5} width={null} height={10} color="#1D94A8" borderRadius={10} />
    )
}

export default FillBar

const styles = StyleSheet.create({})
