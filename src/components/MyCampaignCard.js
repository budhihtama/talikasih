import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import FillBar from './FillBar'

const MyCampaignCard = () => {
    const dummy = [
        {
            image: require('../assets/images/nelayan.jpeg'),
            category: 'Medical',
            title: 'Aid for necessary items to help our country',
            author: 'Aksi Cepat Tanggap',
            raised: 3000,
            goal: 5000
        },
        {
            image:  require('../assets/images/nelayan.jpeg'),
            category: 'Medical',
            title: 'Aid for necessary items to help our country',
            author: 'Aksi Cepat Tanggap',
            raised: 3000,
            goal: 5000
        },
    ]

    return (
    <View>
            {dummy.map((item, index) => (
            <View key={index} style={styles.container}>
                <Image style={{width: '100%', height: 209}} source={item.image}/>
                <View style={styles.content}>
                    <View style={{borderWidth: 1, borderColor: '#A43F3C', padding: 5, marginRight: 'auto', borderRadius: 4}}>
                        <Text style={{color: '#A43F3C'}}>{item.category}</Text>
                    </View>
                        <Text style={{fontWeight: 'bold', fontSize: 17}}>{item.title}</Text>
                        <Text>{item.author}</Text>
                    <View style={{marginTop: 30}}>
                        <FillBar />
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <View>
                                <Text style={{color: 'gray'}}>Raised</Text>
                                <Text style={{color: '#1D94A8'}}>IDR {item.raised}</Text>
                            </View>
                            <View>
                                <Text style={{color: 'gray'}}>Goal</Text>
                                <Text>IDR {item.goal}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
            ))}
    </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 16,
    },
    content: {
        paddingHorizontal: 32,
        marginBottom: 20,
        paddingVertical: 17,
    },
})

export default MyCampaignCard;