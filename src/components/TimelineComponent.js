import React from 'react'
import { StyleSheet, Text, View } from 'react-native';
import Timeline from 'react-native-timeline-flatlist'

const TimelineComponent = () => {
    const data = [
        {time: '09:00', title: 'Event 1', description: 'Event 1 Description, chebfyhabewfyhibaewfhaew ebfhaebfyaewbfebfaewh ehfbaebfyebfyaebf fhebfaewbf'},
        {time: '10:45', title: 'Event 2', description: 'Event 2 Description'},
        {time: '12:00', title: 'Event 3', description: 'Event 3 Description'},
        {time: '14:00', title: 'Event 4', description: 'Event 4 Description'},
        {time: '16:30', title: 'Event 5', description: 'Event 5 Description'}
      ]

    return (
            <Timeline
            data={data}
            // innerCircle={'dot'}
            // separator={true}
            style={{paddingTop: 3}}
            titleStyle={{color: 'black', marginTop: -11}}
            iconStyle={{color: 'red'}}
            circleSize={20}
            circleColor='rgb(45,156,219)'
            lineColor='rgb(45,156,219)'
            descriptionStyle={{color:'black', marginBottom: 20, borderWidth: 1, borderColor: '#ddd', borderRadius: 4, padding: 11}}
            showTime={false}
            />
    )
}

export default TimelineComponent

const styles = StyleSheet.create({})
