import React from 'react'
import { View, Text, StyleSheet, ScrollView, TouchableOpacity } from 'react-native'

const MyDonationCard = () => {
    const dummy = [
        {
            title: 'titlecampaign',
            price: 'rupiah',
            desc: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consectetur, ex voluptates distinctio laudantium ipsa quo voluptatum quam debitis eligendi fuga corporis minima dicta similique. Magnam esse maxime expedita dignissimos voluptates',
            time: 12,
        },
        {
            title: 'titlecampaign',
            price: 'rupiah',
            desc: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consectetur, ex voluptates distinctio laudantium ipsa quo voluptatum quam debitis eligendi fuga corporis minima dicta similique. Magnam esse maxime expedita dignissimos voluptates',
            time: 12,
        },
        {
            title: 'titlecampaign',
            price: 'rupiah',
            desc: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consectetur, ex voluptates distinctio laudantium ipsa quo voluptatum quam debitis eligendi fuga corporis minima dicta similique. Magnam esse maxime expedita dignissimos voluptates',
            time: 12,
        },
    ]

    return (
    <View>
            {dummy.map((item, index) => (
            <View key={index} style={styles.container}>
                <Text style={{alignSelf: 'flex-end'}}>{item.time} minutes ago</Text>
            <Text style={{fontWeight: 'bold', textDecorationLine: 'underline'}}>{item.title}</Text>
            <Text style={{fontSize: 20, color: '#1D94A8'}}>Rp. {item.price}</Text>
            <Text>{item.desc}</Text>
            </View>
            ))}
    </View>
    )
}
const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 30,
        paddingVertical: 24,
        borderRadius: 4,
    },
})

export default MyDonationCard;