import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/FontAwesome'

const ShareComponent = () => {
    return (
        // <View style={{height: '100%'}}>
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.headerText}>Help by sharing</Text>
                    <TouchableOpacity>
                        <Ionicons name='close-outline' size={35} color='#000'/>
                    </TouchableOpacity>
                </View>
                <View style={styles.link}>
                    <Ionicons name='link-outline' size={30} color='#1D94A8'/>
                    {/* <Icon name='link' size={25} color='#1D94A8'/> */}
                    <Text style={styles.linkText}>https://www.google.com</Text>
                </View>
                <TouchableOpacity style={styles.button}>
                    <Text style={styles.buttonText}>COPY LINK</Text>
                </TouchableOpacity>
            </View>
        // </View>
    )
}

export default ShareComponent

const styles = StyleSheet.create({
    container: {
        height: '35%', 
        width: '100%', 
        backgroundColor: 'white', 
        bottom: 0, 
        position: 'absolute', 
        zIndex: 2,
        paddingTop: 16,
        paddingHorizontal: 20,
        paddingBottom: 50
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 22
    },
    headerText: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    link: {
        paddingVertical: 15,
        paddingHorizontal: 15,
        borderBottomWidth: 1,
        borderColor: 'gray',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 17
    },
    linkText: {
        marginLeft: 15,
        fontSize: 18
    },
    button: {
        paddingVertical: 12,
        alignItems: 'center',
        backgroundColor: '#A43F3C',
        borderRadius: 5
    },
    buttonText: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold'
    }
})
