import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const BlurScreen = () => {
    return (
        <View style={{height: '100%', width: '100%', backgroundColor: 'black', opacity: 0.5, position: 'absolute', zIndex: 1}}/>
    )
}

export default BlurScreen

const styles = StyleSheet.create({})
