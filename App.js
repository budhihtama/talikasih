import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

// Screens
import Login from './src/screens/Login';
import DonorDetailsScreen from './src/screens/DonorDetailsScreen';
import Register from './src/screens/Register';
import MyProfile from './src/screens/MyProfile';
import MyProfileEdit from './src/screens/MyProfileEdit';
import MyDonationCard from './src/components/MyDonationCard';
import MyProfileDonations from './src/screens/MyProfileDonations';
import MyProfileCampaign from './src/screens/MyProfileCampaign';
import MyCampaignCard from './src/components/MyCampaignCard';
import DonateScreen from './src/screens/DonateScreen';
import BlurScreen from './src/components/BlurScreen';
import ShareComponent from './src/components/ShareComponent';
import MoreLogin from './src/screens/MoreLogin';
import MoreLogout from './src/screens/MoreLogout';

const App = () => {
  return (
        <View>
          {/* <DonorDetailsScreen /> */}
          <MoreLogout />
        </View>
  );
};

export default App;
